# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:00:59 2021

@author: geomet2
"""

import plotly.express as px
from plotly.offline import plot
import numpy as np
import pandas as pd

l_size = ['a-38µm', 'b+38-53 µm', 'c+53-76µm', 'd+76-106µm',
                 'e+106-152µm','f+152-212µm','g+212-304µm','h+304-424µm','i+424-608µm','j+608-848µm']*10
l_size.sort()

data = {'Size': pd.Series( (v for v in l_size))
        'Mineral' : ['Galena']*100,
        'ID':  list(range(1, 101)),
        'Liberation' : (np.,sqrt(0.02)*np.random.randn(100)+0.6)
        }

df = pd.DataFrame (data)

fig = px.treemap(df, path=['Mineral','Size', 'ID'],
                 values='Liberation',
                 color='Liberation')

fig.update_traces(sort=True)
plot(fig)
fig.show()