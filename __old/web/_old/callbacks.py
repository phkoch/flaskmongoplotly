# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 09:44:22 2021

@author: geomet2
"""

from dash.dependencies import Input, Output

from web import app

@app.callback(
    Output('app-1-display-value', 'children'),
    Input('app-1-dropdown', 'value'))
def display_value(value):
    return 'You have selected "{}"'.format(value)

@app.callback(
    Output('app-2-display-value', 'children'),
    Input('app-2-dropdown', 'value'))
def display_value(value):
    return 'You have selected "{}"'.format(value)