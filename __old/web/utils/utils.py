import numpy as np
from datetime import datetime,timedelta, date
import json
import os # to create an interface with our operating system
import sys # information on how our code is interacting with the host systemimp
import pymongo # for working with the MongoDB API
import pandas as pd
import xml.etree.ElementTree as ET
from bson import json_util
print("Modules loaded")



#loading data
def load_data(mongo_host="mongodb://localhost:27017/",wdb = "Outotec",wcol = "ph0"):
    client = pymongo.MongoClient(mongo_host)
    mydb = client[wdb]
    mycol = mydb[wcol]

    test = mycol.find_one()
    Feed = test['HSCFeedGeo']
    Stream = Feed['HSC8Stream']
    data = Stream['MineralsList']['Mineral']
    # total_confirmed = pd.read_csv(
    #     'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv', encoding='utf-8', na_values=None)
    # total_death = pd.read_csv(
    #     'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv', encoding='utf-8', na_values=None)

    # total_recovered = pd.read_csv(
    #     'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv', encoding='utf-8', na_values=None)
    # total_confirmed.replace(
    #     to_replace='US', value='United States', regex=True, inplace=True)
    # total_recovered.replace(
    #     to_replace='US', value='United States', regex=True, inplace=True)
    # total_death.replace(
    #     to_replace='US', value='United States', regex=True, inplace=True)
    # # I need data that contain population for each country to calculate confirmed cases/population
    # # I download it from : https://github.com/samayo/country-json/blob/master/src/country-by-population.json
    # df_pop = pd.read_json(
    #     'https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-population.json')
    # #some country name has different  format, so I need to change it to match my first dataset
    # df_pop.columns = ['Country/Region', 'population']
    # df_pop = df_pop.replace(to_replace='Russian Federation', value='Russia')


    return data #total_confirmed, total_death, total_recovered, df_pop


