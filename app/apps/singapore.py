import plotly.graph_objects as go
import pandas as pd

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_table
import dash_bootstrap_components as dbc

from app import app

import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import pymongo  # for working with the MongoDB API
import pandas as pd
import dash_core_components as dcc
import plotly.express as px
from app import app
import plotly.graph_objects as go
import pandas as pd
import numpy as np
from plotly.offline import plot

print("Modules loaded")


# MongoDB
print("Connecting to MongoDB...")
# balance le usr:pwd en clair !!
CLIENT = pymongo.MongoClient("mongodb://pickit:28Vx!HypsmGDvjkZ@139.165.65.71:28001")
#CLIENT.Amco_20210223.authenticate('pickit', '28Vx!HypsmGDvjkZ',source="Amco_20210223")

DB = CLIENT["Amco_20210223"]
COLL = DB["analyses"]
TEST = COLL.find_one()
TEST2 = TEST.get("minerals_stats")
PROJ_ROOT = DB["projects"]
PROJ_DB = PROJ_ROOT.find_one()
COLL.find({ '_id': PROJ_DB['analyses'][0] })



print(PROJ_DB)
PROJ_NAME = PROJ_DB
TEST3 = pd.DataFrame(TEST.get("area_percentages"))
print("MongoDB data loaded")
# Feed = test['HSCFeedGeo']
# Stream = Feed['HSC8Stream']
# MineralsList = Stream['MineralsList']['Mineral']

l_size = ['a-38µm', 'b+38-53 µm', 'c+53-76µm', 'd+76-106µm',
                 'e+106-152µm','f+152-212µm','g+212-304µm','h+304-424µm','i+424-608µm','j+608-848µm']*10
l_size.sort()

l_lib00 = (np.sqrt(0.012)*np.random.randn(100)+0.7)
l_lib0 = 0.1*np.round(10*l_lib00)
l_lib = l_lib0/l_lib0.max()
l_lib = np.round(l_lib,1)
#
l_lib.sort()
l_lib = l_lib[::-1]

data = {'Size': pd.Series( (v for v in l_size)),
        'Mineral' : ['Galena']*100,
        'ID':  list(range(1, 101)),
        'Liberation' : l_lib
        }

df = pd.DataFrame (data)
titanic_df = df

# Create dimensions
class_dim = go.parcats.Dimension(
    values=titanic_df.Mineral,
    categoryorder='category ascending', label="Mineral"
)

gender_dim = go.parcats.Dimension(values=titanic_df.Size, label="Size")

survival_dim = go.parcats.Dimension(
    values=titanic_df.Liberation, label="Liberation")

# Create parcats trace
color = titanic_df.Liberation;
colorscale = [[0, 'lightsteelblue'], [1, 'mediumseagreen']];

fig = go.Figure(data = [go.Parcats(dimensions=[class_dim, gender_dim, survival_dim],
        line={'color': color, 'colorscale': colorscale},
        hoveron='color', hoverinfo='count+probability',
        labelfont={'size': 18, 'family': 'Times'},
        tickfont={'size': 16, 'family': 'Times'},
        arrangement='freeform')])