import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import pymongo  # for working with the MongoDB API
import pandas as pd
import dash_core_components as dcc
import plotly.express as px
from app import app

print("Modules loaded")


# MongoDB
print("Connecting to MongoDB...")
CLIENT = pymongo.MongoClient("mongodb://pickit:28Vx!HypsmGDvjkZ@139.165.65.71:28001")
#CLIENT.Amco_20210223.authenticate('pickit', '28Vx!HypsmGDvjkZ',source="Amco_20210223")
DB = CLIENT["Amco_20210223"]
COLL = DB["analyses"]
TEST = COLL.find_one()
TEST2 = TEST.get("minerals_stats")
PROJ_DB = DB["projects"].find_one()
print(PROJ_DB)
PROJ_NAME = PROJ_DB
TEST3 = pd.DataFrame(TEST.get("area_percentages"))
print("MongoDB data loaded")
# Feed = test['HSCFeedGeo']
# Stream = Feed['HSC8Stream']
# MineralsList = Stream['MineralsList']['Mineral']

# DataFrame t2
DATA0 = pd.DataFrame(TEST2)
# Data in lists
LIBERATION_AREA = DATA0["liberation_area_value"]
# liberation_peri = data0["liberation_perimeter_value"]
AREA_PERCENTILE = DATA0["area_percentiles_value"]
PERIMETER_PERCENTILE = DATA0["perimeter_percentiles_value"]
CONVEXITY_PERCENTILE = DATA0["convexity_percentiles_value"]
ELONGATION_PERCENTILE = DATA0["elongation_percentiles_value"]
SIEVE_PERCENTILE = DATA0["sieve_percentiles_value"]
AREA_PERCENTAGE = TEST3.value

EXTERNAL_STYLESHEETS = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

# Dash app
#app = dash.Dash(__name__, external_stylesheets=EXTERNAL_STYLESHEETS)


dataset_lock = pd.DataFrame(PROJ_NAME)

# HTML Layout
layout =html.Div([

                     dcc.Dropdown(id="xaxis-column",
                                  options=[{"label": i, "value": i} for i in DATA0.columns],
                                  value="liberation_area_value",
                                  style={'backgroundColor': '#FFFFFF'},
                                  className='stockselector',
                                  ),

                    dcc.RadioItems(
                       id="xaxis-type",
                       options=[{"label": i, "value": i} for i in ["Linear", "Log"]],
                       value="Linear",
                       labelStyle={"display": "inline-block"},
                   ),

                    html.Div(className='eight columns div-for-charts bg-grey',
                                children=[
                                            dcc.Graph(id="graph-with-slider"),
                    dcc.Dropdown(
                        id="mineral-slider",
                        #multi=True,
                        options=[{"label": i, "value": i} for i in DATA0["mineral_id"]],
                        value=DATA0["mineral_id"].min(),
                    ),
                    ]),
                    ])
        # TODO : particule ici
        # html.Img(src=get_asset_url(r"Clipboard01.png"),height='128', width='128')
    #     dbc.Card(
    # [
    #     dbc.CardImg(src='C:/Users/geomet2/Desktop/Clipboard01.png', top=True),
    #     dbc.CardBody(
    #         [
    #             html.H3("Card title", className="card-title"),
    #             html.P(
    #                 "Some quick example text to build on the card title and "
    #                 "make up the bulk of the card's content.",
    #                 className="card-text",
    #             ),
    #             dbc.Button("Go somewhere", color="primary"),
    #         ]
    #     ),
#     ],
#     style={"width": "28rem"},
# ),

        # ])

@app.callback(
    Output("graph-with-slider", "figure"),
    Input("mineral-slider", "value"),
    Input("xaxis-column", "value"),
    Input("xaxis-type", "value"),
)
def update_figure(selected_mineral, xaxis_column_name, xaxis_type):
    """


    Parameters
    ----------
    selected_mineral : INT
        Mineral_ID [200]
    xaxis_column_name : STR
        x-axis labels.
    xaxis_type : BOOL
        Linear or log x-axis (not y-axis)

    Returns
    -------
    fig : TYPE
        Plolty figure

    """
    filtered_df = DATA0[DATA0.mineral_id == selected_mineral]
    fig = px.line(y=list(filtered_df[xaxis_column_name]))
    fig.update_xaxes(
        title=str(xaxis_column_name) + "%",
        type="linear" if xaxis_type == "Linear" else "log",
    )
    # Set x-axis title
    # fig.update_xaxes(title_text="xaxis title")

    # Set y-axes titles
    fig.update_yaxes(
        title_text="<b>x 100 % Area</b>", secondary_y=False
    )

    annotations = []
    annotations.append(
        dict(
            xref="paper",
            yref="paper",
            x=0.0,
            y=1.05,
            xanchor="left",
            yanchor="bottom",
            text="Minerals Dashboard",
            font=dict(family="Arial", size=35, color="rgb(37,37,37)"),
            showarrow=False,
        )
    )
    fig.update_layout(transition_duration=20, annotations=annotations)

    return fig

# needed only if running this as a single page app
# if __name__ == '__main__':
#     app.run_server(host='127.0.0.1', debug=True)